set -e
./switchBuildsToRelease.sh
buildScripts/native/tests/defaultRun.sh

cp /home/demensdeum/Sources/cube-art-project-bootstrap/cube-art-project/tests/cubeArtProjectTests/cubeArtProjectTests/cubeArtProjectTests release
cp /usr/local/lib/libCubeArtProject.so release
cp /usr/local/lib/libFlameSteelBattleHorn.so release
cp /usr/local/lib/libFlameSteelCommonTraits.so release
cp /usr/local/lib/libFlameSteelCore.so release
cp /usr/local/lib/libFlameSteelEngineGameToolkitFSGL.so release
cp /usr/local/lib/libFlameSteelEngineGameToolkit.so release
cp /usr/local/lib/libFSGL.so release

cd release
zip -r CubeArtProjectLinux86_64.zip cubeArtProjectTests *.so
rm cubeArtProjectTests
rm *.so
cd ..
