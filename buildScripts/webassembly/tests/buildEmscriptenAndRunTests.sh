set -e
set -x
buildScripts/webassembly/emscriptenBuild.sh

rm -rf /var/www/html/tests || true
mkdir /var/www/html/tests

cd cube-art-project
cd tests

for directory in */;
do
cd $directory
if [[ $1 == "-defaultTestRunner" ]]; then
./buildEmscripten.sh -defaultTestRunner
else
./buildEmscripten.sh
fi
cd ..
done
cd ..
cd ..

cd cube-art-project
cd tests
for directory in */;
do
mkdir /var/www/html/tests/${directory}
cp ../src/webassemblyResources/* /var/www/html/tests/${directory}
cp ${directory}${directory}${directory%/}.js /var/www/html/tests/${directory}/main.js
cp ${directory}${directory}*.wasm /var/www/html/tests/${directory}
cp ${directory}${directory}*.data /var/www/html/tests/${directory} || true
if [[ $1 == "-defaultTestRunner" ]]; then
python3 ${directory}/src/main.py 
fi
done
cd ..
cd ..
