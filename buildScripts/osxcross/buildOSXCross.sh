source ~/Apps/osxcross.sh
export OSXCROSS_MP_INC=1
export OSXCROSS_NO_INCLUDE_PATH_WARNINGS=1
./frameworkCompileInstall.sh cleanBuilds compileOSXCross
cd cube-art-project
cd tests

for directory in */;
do

cd $directory
rm CMakeCache.txt
rm cmake_install.cmake
rm -rf CMakeFiles
cmake -DOSXCROSS=1 .
make
cd ..

done
