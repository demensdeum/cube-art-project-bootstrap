set -e
set -x

rm -rf release || true
mkdir release

for directory in buildScripts/packRelease/*;
do
    $directory/testAndPack.sh
done 
