./switchBuildsToRelease.sh
set -e
set -x
rm -rf release/webassembly || true
mkdir release/webassembly
buildScripts/webassembly/tests/defaultRun.sh
cp cube-art-project/tests/cubeArtProjectTests/cubeArtProjectTests/cubeArtProjectTests.js release/webassembly/main.js
cp cube-art-project/tests/cubeArtProjectTests/cubeArtProjectTests/cubeArtProjectTests.wasm release/webassembly/cubeArtProjectTests.wasm
cp cube-art-project/tests/cubeArtProjectTests/cubeArtProjectTests/cubeArtProjectTests.data release/webassembly/cubeArtProjectTests.data
cp cube-art-project/src/webassemblyResources/indexFullscreen.html release/webassembly/index.html
python3 buildScripts/packRelease/webassembly/argumentsHandlingInjection.py
