set -e
set -x

./switchBuildsToRelease.sh

/home/demensdeum/Android/Sdk/emulator/emulator -avd Pixel_2_API_24 &

buildScripts/android/tests/defaultRun.sh -defaultTestRunner

~/Android/Sdk/platform-tools/adb logcat -c
~/Android/Sdk/platform-tools/adb shell am start -a android.intenon.MAIN -n org.libsdl.app/org.libsdl.app.SDLActivity

sleep 20

~/Android/Sdk/platform-tools/adb shell logcat -d  > cube-art-project/build/android/androidTestRun.log

grep "screenshot checks passed" cube-art-project/build/android/androidTestRun.log

buildScripts/android/tests/defaultRun.sh

sleep 10

cp cube-art-project/build/android/android-project/app/build/outputs/apk/debug/app-debug.apk release

mv release/app-debug.apk release/CubeArtProject.apk
