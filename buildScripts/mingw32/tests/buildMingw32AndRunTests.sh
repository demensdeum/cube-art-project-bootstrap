set -e
set -x
buildScripts/mingw32/tests/mingw32build.sh

if [ -d windowsTestsPlayground ]; then
    rm -rf windowsTestsPlayground
fi
mkdir windowsTestsPlayground

cd FlameSteelFramework
declare -a FlameSteelFrameworkLibraries=("FlameSteelCore" "FlameSteelBattleHorn" "FlameSteelCommonTraits" "FlameSteelEngineGameToolkit" "FSGL" "FlameSteelEngineGameToolkitFSGL" "../cube-art-project/sharedLib/")

for directory in ${FlameSteelFrameworkLibraries[@]}; do
    cd $directory
    cp *.dll ../../windowsTestsPlayground
    cd ..
done

cd ..

cd cube-art-project
cd tests

for directory in */;
do

cd $directory
rm CMakeCache.txt || true
rm cmake_install.cmake || true
rm -rf CMakeFiles || true
cmake -DMINGW32=1 .
make -j 8
cp data/* ../../../windowsTestsPlayground || true
cp $directory*.exe ../../../windowsTestsPlayground
cp ~/Sources/cube-art-project-bootstrap/windowsSDLDependencies/*.dll ../../../windowsTestsPlayground
cd ..

done

cd /home/demensdeum/Sources/cube-art-project-bootstrap/windowsTestsPlayground/
wine cubeArtProjectTests.exe -defaultTestRunner
cd ..
