#!/bin/bash

set -x
set -e

export ANDROID_HOME=/home/demensdeum/Android/Sdk/
export ANDROID_NDK_HOME=/home/demensdeum/Android/android-ndk-r21-beta2/

rm -rf cube-art-project/build/android || true
mkdir cube-art-project/build/android

cp -R buildScripts/android/android-project cube-art-project/build/android
mkdir cube-art-project/build/android/android-project/app/jni/include

cp -R FlameSteelFramework/FlameSteelBattleHorn/src/FlameSteelBattleHorn cube-art-project/build/android/android-project/app/jni/include/FlameSteelBattleHorn/
cp -R FlameSteelFramework/FlameSteelCommonTraits/src/FlameSteelCommonTraits cube-art-project/build/android/android-project/app/jni/include/FlameSteelCommonTraits/
cp -R FlameSteelFramework/FlameSteelCore/src/FlameSteelCore cube-art-project/build/android/android-project/app/jni/include/FlameSteelCore/
cp -R FlameSteelFramework/FlameSteelEngineGameToolkit/src/FlameSteelEngineGameToolkit cube-art-project/build/android/android-project/app/jni/include/FlameSteelEngineGameToolkit/
cp -R FlameSteelFramework/FSGL/src/FSGL cube-art-project/build/android/android-project/app/jni/include/FSGL/
cp -R FlameSteelFramework/FlameSteelEngineGameToolkitFSGL/src/FlameSteelEngineGameToolkitFSGL cube-art-project/build/android/android-project/app/jni/include/FlameSteelEngineGameToolkitFSGL/

mkdir cube-art-project/build/android/android-project/app/src/main/assets/
cp cube-art-project/tests/cubeArtProjectTests/data/* cube-art-project/build/android/android-project/app/src/main/assets/
cp -R cube-art-project/src/CubeArtProject/ cube-art-project/build/android/android-project/app/jni/include/
cp -R cube-art-project/include/* cube-art-project/build/android/android-project/app/jni/include/
cp cube-art-project/build/android/android-project/app/jni/SDL_image/*.h cube-art-project/build/android/android-project/app/jni/SDL/include
cp cube-art-project/build/android/android-project/app/jni/SDL_mixer/*.h cube-art-project/build/android/android-project/app/jni/SDL/include

#---

mkdir -p cube-art-project/build/android/android-project/app/jni/FlameSteelBattleHorn/src/
cp -R FlameSteelFramework/FlameSteelBattleHorn/src/ cube-art-project/build/android/android-project/app/jni/FlameSteelBattleHorn/
cp FlameSteelFramework/FlameSteelBattleHorn/Android.mk cube-art-project/build/android/android-project/app/jni/FlameSteelBattleHorn/

mkdir -p cube-art-project/build/android/android-project/app/jni/FlameSteelCommonTraits/src/
cp -R FlameSteelFramework/FlameSteelCommonTraits/src/ cube-art-project/build/android/android-project/app/jni/FlameSteelCommonTraits/
cp FlameSteelFramework/FlameSteelCommonTraits/Android.mk cube-art-project/build/android/android-project/app/jni/FlameSteelCommonTraits/

mkdir -p cube-art-project/build/android/android-project/app/jni/FlameSteelCore/src/
cp -R FlameSteelFramework/FlameSteelCore/src/ cube-art-project/build/android/android-project/app/jni/FlameSteelCore/
cp FlameSteelFramework/FlameSteelCore/Android.mk cube-art-project/build/android/android-project/app/jni/FlameSteelCore/

mkdir -p cube-art-project/build/android/android-project/app/jni/FlameSteelEngineGameToolkit/src/
cp -R FlameSteelFramework/FlameSteelEngineGameToolkit/src/ cube-art-project/build/android/android-project/app/jni/FlameSteelEngineGameToolkit/
cp FlameSteelFramework/FlameSteelEngineGameToolkit/Android.mk cube-art-project/build/android/android-project/app/jni/FlameSteelEngineGameToolkit/

mkdir -p cube-art-project/build/android/android-project/app/jni/FSGL/src/
cp -R FlameSteelFramework/FSGL/src/ cube-art-project/build/android/android-project/app/jni/FSGL/
cp FlameSteelFramework/FSGL/Android.mk cube-art-project/build/android/android-project/app/jni/FSGL/

mkdir -p cube-art-project/build/android/android-project/app/jni/FlameSteelEngineGameToolkitFSGL/src/
cp -R FlameSteelFramework/FlameSteelEngineGameToolkitFSGL/src/ cube-art-project/build/android/android-project/app/jni/FlameSteelEngineGameToolkitFSGL/
cp FlameSteelFramework/FlameSteelEngineGameToolkitFSGL/Android.mk cube-art-project/build/android/android-project/app/jni/FlameSteelEngineGameToolkitFSGL/

mkdir -p cube-art-project/build/android/android-project/app/jni/CubeArtProject/src
cp -R cube-art-project/src/CubeArtProject/ cube-art-project/build/android/android-project/app/jni/CubeArtProject/src
cp buildScripts/android/Android.mk cube-art-project/build/android/android-project/app/jni/CubeArtProject/

cp -R cube-art-project/build/android/android-project/app/jni/SDL/include cube-art-project/build/android/android-project/app/jni/include/SDL2

rm cube-art-project/build/android/android-project/app/jni/src/main.cpp
cp cube-art-project/tests/cubeArtProjectTests/src/* cube-art-project/build/android/android-project/app/jni/src/

if [[ $1 == "-defaultTestRunner" ]]; then
rm cube-art-project/build/android/android-project/app/jni/src/Android.mk
mv cube-art-project/build/android/android-project/app/jni/src/AndroidTest.mk cube-art-project/build/android/android-project/app/jni/src/Android.mk
fi


cd cube-art-project/build/android/android-project/
#./gradlew clean
./gradlew build
./gradlew installDebug
./gradlew assemble
