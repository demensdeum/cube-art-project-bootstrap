set -e
set -x
./switchBuildsToRelease.sh
buildScripts/mingw32/tests/defaultRun.sh
cd windowsTestsPlayground
zip -r CubeArtProjectApplication.zip *.exe *.dll
mv CubeArtProjectApplication.zip ../release/CubeArtProjectWin32.zip
cd ..
