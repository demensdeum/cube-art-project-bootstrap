set -e

if [ $# -eq 0 ]
  then
    echo "No arguments provided, should be \"cleanNativeSystem\" or \"cleanBuilds\" or \"compileNative\" or \"compileEmscripten\""
    exit 1
fi

for flag in "$@"; do
    if [[ $* == "cleanNativeSystem" ]]
        then
            sudo rm -rf /usr/local/include/FlameSteelFramework
    fi
done

cd FlameSteelFramework
declare -a FlameSteelFrameworkLibraries=("FlameSteelCore" "FlameSteelBattleHorn" "FlameSteelCommonTraits" "FlameSteelEngineGameToolkit" "FSGL" "FlameSteelEngineGameToolkitFSGL" "../cube-art-project/sharedLib/")

for directory in ${FlameSteelFrameworkLibraries[@]}; do
    echo "Work with $directory"
    cd $directory
    for flag in "$@"; do
        if [[ $flag == "cleanBuilds" ]]
            then 
                rm -rf CMakeFiles
                rm -f CMakeCache.txt
                rm -f install_manifest.txt
                rm -f cmake_install.cmake
                rm *.dll || true
                rm *.so || true
        fi
    done
    for flag in "$@"; do
        if [[ $flag == "compileNative" ]]
            then 
                cmake .
                make -j 8
        fi
    done
    for flag in "$@"; do
        if [[ $flag == "nativeInstall" ]]
            then 
                sudo make install
                sudo ldconfig
        fi
    done    
    for flag in "$@"; do
        if [[ $flag == "compileEmscripten" ]]
            then
                emcmake cmake -DEMSCRIPTEN=1 .
                emmake make -j 8
        fi
    done
    for flag in "$@"; do
        if [[ $flag == "compileMinGW32" ]]
            then
                cmake -DCMAKE_CXX_FLAGS=-I\ /home/demensdeum/Sources/cube-art-project-bootstrap/windowsSDLDependencies/includes -DMINGW32=1 .
                make -j 8
        fi
    done
    for flag in "$@"; do
        if [[ $flag == "compileOSXCross" ]]
            then
                cmake -DOSXCROSS=1 .
                make -j 8
                ./osxCrossRebuild.sh
        fi
    done        
    cd ..
done

exit 0
