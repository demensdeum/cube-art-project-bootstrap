set -e
source ~/Sources/emsdk/emsdk_env.sh
./frameworkCompileInstall.sh cleanNativeSystem cleanBuilds compileNative nativeInstall
if [ ! -L $EMSDK/fastcomp/emscripten/system/include/FlameSteelFramework ]; then
    echo $EMSDK/fastcomp/emscripten/system/include/FlameSteelFramework
    sudo ln -s /usr/local/include/FlameSteelFramework $EMSDK/fastcomp/emscripten/system/include/FlameSteelFramework
fi
if [ ! -L $EMSDK/fastcomp/emscripten/system/include/CubeArtProject ]; then
    echo $EMSDK/fastcomp/emscripten/system/include/CubeArtProject
    sudo ln -s /usr/local/include/CubeArtProject $EMSDK/fastcomp/emscripten/system/include/CubeArtProject
fi
./frameworkCompileInstall.sh cleanBuilds compileEmscripten
