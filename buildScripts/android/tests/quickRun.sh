#!/bin/bash

set -x
set -e

export ANDROID_HOME=/home/demensdeum/Android/Sdk/
export ANDROID_NDK_HOME=/home/demensdeum/Android/android-ndk-r21-beta2/

cd cube-art-project/build/android/android-project/

./gradlew build
./gradlew installDebug
./gradlew assemble
