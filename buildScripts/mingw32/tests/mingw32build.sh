set -e
./frameworkCompileInstall.sh cleanNativeSystem cleanBuilds compileNative nativeInstall

if [ ! -L /usr/i686-w64-mingw32/include/FlameSteelFramework ]; then
    echo /usr/i686-w64-mingw32/include/FlameSteelFramework
    sudo ln -s /usr/local/include/FlameSteelFramework /usr/i686-w64-mingw32/include/FlameSteelFramework
fi
if [ ! -L /usr/i686-w64-mingw32/include/CubeArtProject ]; then
    echo /usr/i686-w64-mingw32/include/CubeArtProject
    sudo ln -s /usr/local/include/CubeArtProject /usr/i686-w64-mingw32/include/CubeArtProject
fi

./frameworkCompileInstall.sh cleanBuilds compileMinGW32
