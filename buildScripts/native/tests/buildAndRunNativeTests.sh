set -e
set -x

cd cube-art-project
cd tests

for directory in */;
do

cd $directory
rm CMakeCache.txt || true
rm cmake_install.cmake || true
rm -rf CMakeFiles || true
cmake .
make
cd $directory
./${directory%/} -defaultTestRunner
cd ..
cd ..

done 
